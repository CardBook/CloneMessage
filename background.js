async function main() {
	async function cloneMessage(tab, info) {
		let composeDetail = await messenger.compose.getComposeDetails(tab.id);
		if (composeDetail.isPlainText) {
			if (composeDetail.body) {
				// can't make it work
				// let parser = new DOMParser();
				// let htmlDoc = parser.parseFromString(composeDetail.body, 'text/html');
				let body = composeDetail.body.replace(/(.*)<body([^>]*)>/, "").replace(/<\/body>(.*)/, "");
				body = body.replace(/<br>/g, "\n");
				// signature
				body = body.replace(/<div class=\"moz-signature\">/g, "");
				body = body.replace(/<\/div>/g, "");
				composeDetail.plainTextBody = body;
			}
			composeDetail.body = null;
		} else {
			composeDetail.plainTextBody = null;
		}
		let tab1 = await messenger.compose.beginNew(composeDetail);

		let listAttachments = await messenger.compose.listAttachments(tab.id);
		for (let attachment of listAttachments) {
			// thanks John !
			let file = "getAttachmentFile" in messenger.compose
				? await messenger.compose.getAttachmentFile(attachment.id)
				: await attachment.getFile();
			await messenger.compose.addAttachment(tab1.id, {file: file, name: attachment.name});
		}
	};

	browser.composeAction.onClicked.addListener(cloneMessage);
};

main();
